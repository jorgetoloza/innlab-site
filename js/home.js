var morphing = anime({
 	targets: '#hero .logo .liquid',
  	d: [
	    {
	    	value: 'M574.3,773.9c-54.3,77.1-160.9,95.5-238,41.2c-44.5-31.4-62.7-72.8-67.7-97.4c-3.6-18-0.7-3.7,29.3-8.7 c2.9-0.5,27.4-4.6,29.4-4.3c20.2,3,26.3,6.3,40.6,7c29,1.4,28.1-26.5,84.3-14c13.4,9.4,35.6-2.8,49.3,5.3 C567.1,742,619.2,710.1,574.3,773.9z'
	    },
	    {
	    	value: 'M574.3,773.9c-54.3,77.1-160.9,95.5-238,41.2c-44.5-31.4-62.7-72.8-67.7-97.4c-3.6-18-0.7-3.7,29.3-8.7c2.9-0.5,27.4-4.6,29.4-4.3c20.2,3,26.3,6.3,40.6,7c29,1.4,28.1-26.5,84.3-14c13.4,9.4,35.6-2.8,49.3,5.3C568.7,709.3,619.2,710.1,574.3,773.9z'
	    },
	    {
	    	value: 'M574.3,773.9c-54.3,77.1-160.9,95.5-238,41.2c-44.5-31.4-62.7-72.8-67.7-97.4c-3.6-18-0.7-3.7,29.3-8.7c2.9-0.5,27.4-4.6,29.4-4.3c20.2,3,26.3,6.3,40.6,7c29,1.4,28.8-5,84.3-14c20.5-7,49.1,9,65,6.7C584.3,710.8,619.2,710.1,574.3,773.9z'
	    },
	    {
	    	value: 'M574.3,773.9c-54.3,77.1-160.9,95.5-238,41.2c-44.5-31.4-62.7-72.8-67.7-97.4c-3.6-18-0.7-3.7,29.3-8.7 c2.9-0.5,27.7,0.9,29.7,1.2c20.2,3,26,0.8,40.3,1.5c29,1.4,34.4-49.2,84.3-14c13.4,9.4,35.6-2.8,49.3,5.3 C567.1,742,619.2,710.1,574.3,773.9z'
	    }
	],
	easing: 'linear',
	duration: 8000,
	loop: true
});
var iconsDrawingAnime = {};

$('#services .service').each(function(i, obj){
	var config = {
		targets: $(obj).find('path')[0],
		strokeDashoffset: [anime.setDashoffset, 0],
		easing: 'easeInOutSine',
		autoplay: false,
		duration: 2500,
		delay: function(el, i) { return i * 250 },
		direction: 'alternate',
		loop: true
	};
	iconsDrawingAnime[$(obj).attr('name')] = anime(config);
});
function changeRandomImage(){
	var $p = $($('#works .project')[Math.floor(Math.random() * $('#works .project').length)]);
	var $images = $p.find('.img.active').siblings('.img');
	$p.find('.img.active').removeClass('active');
	$($images[Math.floor(Math.random() * $images.length)]).addClass('active');
}
function changeClient(){
	var nuevo = Math.floor((Math.random() * 11) + 1);
	while($('#clients .img[data-active="' + nuevo + '"]').length != 0)
		nuevo = Math.floor((Math.random() * 11) + 1);
	var $img = $($('#clients .img')[Math.floor(Math.random() * $('#clients .img').length)]);
	$img.css('opacity', 0);
	setTimeout(function(){
		$img.css({
			'background-image': 'url("img/clients/' + nuevo + '.png")',
			'opacity': 1
		}).attr('data-active', nuevo);
	}, 1500);
}
function changeAboutIcons(){
	var $item = $($('#about .item')[Math.floor(Math.random() * $('#about .item').length)]);
	var $slides = $item.find('.slide.active').siblings('.slide');
	$item.find('.slide.active').removeClass('active');
	$($slides[Math.floor(Math.random() * $slides.length)]).addClass('active');
}
setInterval(changeRandomImage, 7000);
setInterval(changeClient, 2000);
setInterval(changeAboutIcons, 5000);


$(document).ready(function(){
	var delay = 0.1;
	$('#menu').children().each(function(){
		$(this).find('span, a, .title').each(function(i, obj){
			$(obj).css('transition', 'opacity 1.5s cubic-bezier(0,0,0.09,0.87) ' + delay + 's, transform 1.5s cubic-bezier(0,0,0.09,0.87) ' + delay + 's');
			delay+= 0.1;
		});
		delay = 0.1;
	});
	delay = 0.3;
	$(window).on('scroll', function(){
		if($(window).scrollTop() > $('footer').offset().top){
			$('header').addClass('transparent');
		}else{
			$('header').removeClass('transparent');
		}
		if($(window).scrollTop() > 80){
			$('body').addClass('sticky');
		}else{
			$('body').removeClass('sticky');
		}
	});
	$(window).trigger('scroll');
	$('#menuToggle').on('click', function(){
		$('body').toggleClass('menu-show');
		$('#menu').toggleClass('show');
		setTimeout(function(){
			$('#menuToggle').toggleClass('can-close');
		}, 1000);
	});
	$('#menu .menu-item').on('click', function(){
		var to = $(this).attr('href');
		$('#menuToggle').trigger('click');
		$('html, body').animate({
        	scrollTop: $(to).offset().top - $('header').height()
        }, 'slow');
	});
	$('#toAbout').on('click', function(){
		$('html').animate({
		    scrollTop: $('section#about').offset().top
		}, 1000);
	});
	$('#services .service').on('mouseover', function(e){
		iconsDrawingAnime[$(this).attr('name')].play();
	});
	$('#services .service').on('mouseout', function(){
		iconsDrawingAnime[$(this).attr('name')].pause();
	});
});
